/**
 * Creamos nuestro juego instanciando la clase Phaser
 * @param {int} 810              [ancho]
 * @param {int} 810              [alto]
 * @param {obj} Phaser.AUTO      [tipo de motor]
 * @param {string} "game-container" [id del div que contendrá el canvas]
 */
var MyGame = new Phaser.Game(780, 810, Phaser.AUTO, "game-container");

// Agregamos todos los estados
MyGame.state.add("BootState", BootState);
MyGame.state.add("PreloaderState", PreloaderState);
MyGame.state.add("GameState", GameState);

// Iniciamos el estado BootState
MyGame.state.start("BootState");
