var GameState = {
  init: function() {
    // Personaje
    this.PEDALEANDO = false;
    this.PEDALEO_ANTERIOR = null;

    // Indica si el mundo se encuentra girando o no
    this.MUNDO_GIRANDO = false;

    // Sonido del nivel se encuentra encendido o no
    this.SONIDO_AMBIENTAL_ON = true;

  },
  // Precarga todos nuestros elementos de nuestro juego
  preload: function() {

  },
  // Agrega los elementos a nuestro escenario
  create: function() {

    // Imagen de fondo
    this.fondo = this.game.add.image(0, 0, "ambiente_lima");

    // Sonido ambiental
    this.sonido_ambiental = this.game.add.audio("sonido_ambiental");
    this.sonido_ambiental.volume = 0.2;
    this.sonido_ambiental.loopFull();

    // Agregamos el cartel del nivel
    this.cartel_nivel = this.game.add.sprite(this.game.world.width - 148, 70, "cartel_nivel", 0);
    this.cartel_nivel.anchor.set(0.5, 0.5);


    // Agregamos las capas del planeta
    this.mundo_back = this.game.add.sprite(this.game.world.centerX, this.game.world.height-160, "mundo_lima", 2);
    this.mundo_back.anchor.set(0.5, 0.5);

    this.mundo_mid = this.game.add.sprite(this.game.world.centerX, this.game.world.height-160, "mundo_lima", 1);
    this.mundo_mid.anchor.set(0.5, 0.5);

    this.mundo_front = this.game.add.sprite(this.game.world.centerX, this.game.world.height-160, "mundo_lima", 0);
    this.mundo_front.anchor.set(0.5, 0.5);

    // Sprite para el control del sonido ambiental
    this.control_sonido_ambiental = this.game.add.sprite(30, 30, "control_sonido_ambiental");
    this.control_sonido_ambiental.inputEnabled = true;
		this.control_sonido_ambiental.events.onInputDown.add(function(){

      if (this.SONIDO_AMBIENTAL_ON === true) {
        this.sonido_ambiental.pause();
        this.control_sonido_ambiental.frame = 1;
        this.SONIDO_AMBIENTAL_ON = false;

      } else {
        this.sonido_ambiental.resume();
        this.control_sonido_ambiental.frame = 0;
        this.SONIDO_AMBIENTAL_ON = true;
      }

    }, this);

    // Sonido del pedaleo
    this.sonido_pedaleo = this.game.add.audio("sonido_pedaleo");
    this.sonido_pedaleo.volume = 0.2;

    // Cuerpo del personaje
    this.cuerpo_ciclista = this.game.add.sprite(this.game.world.centerX+2, 328, "cuerpo_personaje");
    this.cuerpo_ciclista.anchor.set(0.5, 0.5);
    this.pedaleo_left = this.cuerpo_ciclista.animations.add("pedaleo_left", [0,1,2], 9, false);
    this.pedaleo_right = this.cuerpo_ciclista.animations.add("pedaleo_right", [3,4,5], 9, false);

    // Cabeza del personaje
    this.cabeza_personaje = this.game.add.sprite(this.game.world.centerX, 260, "cabeza_personaje");
    this.cabeza_personaje.anchor.set(0.5, 0.5);

    // Activamos las teclas
    this.teclas = this.game.input.keyboard.addKeys({
      LEFT: Phaser.KeyCode.LEFT,
      RIGHT: Phaser.KeyCode.RIGHT
    });

  },
  // Refresca constantemente el canvas
  update: function() {

    // Verificamos si se presionan las tecla
    this.deteccionTeclas();

  },
  deteccionTeclas : function() {
    // Pedaleo usando las teclas
    if (this.teclas.LEFT.isDown && this.PEDALEANDO === false && this.PEDALEO_ANTERIOR !== "left") {
      this.sonido_pedaleo.play();
      this.pedaleo_left.play("pedaleo_left");
      this.PEDALEANDO = true;
      this.rotacionMundo();

      // Cambiamos expresión del personaje
      this.cabeza_personaje.frame = 9;
      this.contadorCambioRostro();

      this.pedaleo_left.onComplete.add(function() {
        this.sonido_pedaleo.stop();
        this.PEDALEO_ANTERIOR = "left";
        this.PEDALEANDO = false;

      }, this);

    }
    else if (this.teclas.RIGHT.isDown && this.PEDALEANDO === false && this.PEDALEO_ANTERIOR !== "right") {

      this.sonido_pedaleo.play();
      this.pedaleo_right.play("pedaleo_right");
      this.PEDALEANDO = true;
      this.rotacionMundo();

      this.cabeza_personaje.frame = 9;
      this.contadorCambioRostro();

      this.pedaleo_right.onComplete.add(function() {
        this.sonido_pedaleo.stop();
        this.PEDALEO_ANTERIOR = "right";
        this.PEDALEANDO = false;

      }, this);

    }
  },
  rotacionMundo: function() {
    this.animacionGirarMundoFront = this.game.add.tween(this.mundo_front);
    this.animacionGirarMundoFront.to({angle: "-20"}, 500);
		this.animacionGirarMundoFront.start();

    this.animacionGirarMundoMid = this.game.add.tween(this.mundo_mid);
    this.animacionGirarMundoMid.to({angle: "-8"}, 500);
		this.animacionGirarMundoMid.start();

    this.animacionGirarMundoBack = this.game.add.tween(this.mundo_back);
    this.animacionGirarMundoBack.to({angle: "-1"}, 500);
		this.animacionGirarMundoBack.start();

  },
  contadorCambioRostro: function(num_frame) {
    if (this.contador_cambio_rostro) {
      this.time.events.remove(this.contador_cambio_rostro);
      console.log("Eliminamos el contador que ya existia");
    }
    this.contador_cambio_rostro = this.time.events.add(Phaser.Timer.SECOND * 1, function(){
      console.log("regresamos al rostro por defecto");
      this.cabeza_personaje.frame = 0;
    }, this);

  }
};
