var BootState = {
  init: function() {
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.pageAlignVertically = true;
    this.scale.pageAlignHorizontally = true;
  },
  preload: function() {
    // Assets de la pantalla del preloader
    this.load.spritesheet("personaje_preloader", "img/personaje-preloader.png", 68, 112, 6);
  },
  create: function() {
    
    this.game.stage.backgroundColor = "#c1c2c4";
    MyGame.state.start("PreloaderState");
  }
};
