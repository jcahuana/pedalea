var PreloaderState = {
  preload: function() {
    // Imagen del cargador
    this.presonaje_preloader = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, "personaje_preloader");
    this.presonaje_preloader.anchor.setTo(0.5, 0.5);

    // Ambientes de cada nivel
    this.load.image("ambiente_lima", "img/bg-lima.jpg");

    // Sprite de control de sonido ambiental
    this.load.spritesheet("control_sonido_ambiental", "img/control-sonido-ambiental.png", 34, 26, 2);

    // Cartel del nivel
    this.load.spritesheet("cartel_nivel", "img/carteles-niveles.png", 296, 305, 3);

    // Agregamos el mundo
    // Params: identificador, url, ancho del frame, alto del frame
    this.load.spritesheet("mundo_lima", "img/mundo-lima.png", 1000, 1000, 5);

    // Cabeza del personaje
    this.load.spritesheet("cabeza_personaje", "img/cabeza-ciclista.png", 97, 82, 18);

    // Cuerpo del personaje
    this.load.spritesheet("cuerpo_personaje", "img/ciclista.png", 91, 75, 6);

    // Sonidos
    this.load.audio("sonido_pedaleo", ["sounds/pedaleo.mp3", "sounds/pedaleo.ogg"]);
    this.load.audio("sonido_ambiental", ["sounds/ambiental_lima.mp3", "sounds/ambiental_lima.ogg"]);

    this.anim_preloader = this.presonaje_preloader.animations.add("pedaleo_preloader", [0,1,2,3,4,5], 6, true);
    this.anim_preloader.play("pedaleo_preloader");

    MyGame.load.onLoadComplete.add(function() {
      this.anim_preloader.destroy();
    }, this);

  },
  create: function() {
    MyGame.state.start("GameState");
  }
};
